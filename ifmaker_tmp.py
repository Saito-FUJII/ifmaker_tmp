import openpyxl
import os, tkinter, tkinter.filedialog, tkinter.messagebox


def const_val_writer(x):
    if(x[2].value.startswith('s')):
        print('#define X(x)    (({}g_blround(x)) / ({}))'.format(x[2].value, x[3].value))
        print('volatile const {} {} = X({})'.format(x[2].value, x[0].value, x[6].value))
        print('#undef X')

    else :
        print('#define X(x)    ()')
        print('volatile const {} {} = {}'.format(x[2].value, x[0].value, x[6].value))


# workbook = openpyxl.load_workbook('sample.xlsx')
# sheet = workbook['定数']

# sheet_range = sheet['A4:N12']


# ファイル選択ダイアログの表示
root = tkinter.Tk()
root.withdraw()
fTyp = [("","*")]
iDir = os.path.abspath(os.path.dirname(__file__))
tkinter.messagebox.showinfo('○×プログラム','処理ファイルを選択してください！')

file = tkinter.filedialog.askopenfilename(filetypes = fTyp,initialdir = iDir)


workbook = openpyxl.load_workbook(file.read())
sheet = workbook['定数']

sheet_range = sheet['A4:N12']

for row in sheet_range:
    if(row[11].value == '〇'):
        pass
    elif(row[11].value == '✕'):
        const_val_writer(row)
    else :
        print("ILLEGAL INPUT IN {} CELL", row[11].coordinate)


workbook.close()


